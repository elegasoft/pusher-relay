<?php

namespace PusherRelay;

use Illuminate\Routing\Controller;

class PusherRelayController extends Controller
{

    public function store(PusherRelayRequest $request)
    {
        if (PusherRelay::send($request)) {
            return ['message' => 'success'];
        }
        return ['error' => 'An unknown message occurred'];
    }

}