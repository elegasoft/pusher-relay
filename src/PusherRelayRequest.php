<?php

namespace PusherRelay;

use Illuminate\Foundation\Http\FormRequest;

class PusherRelayRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'channel' => 'required',
            'event' => 'required',
            'data' => 'required|array',
        ];
    }
}