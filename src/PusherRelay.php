<?php

namespace PusherRelay;

use Illuminate\Support\Facades\Config;
use Pusher\Pusher;

class PusherRelay
{
    /**
     * Sends the notification to Pusher
     *
     * @param PusherRelayRequest|\Illuminate\Http\Request $request
     * @return array|bool
     */
    public static function send(PusherRelayRequest $request)
    {
        $config = Config::get('broadcasting.connections.pusher');
        $pusher = new Pusher($config['key'], $config['secret'], $config['app_id'], $config['options']);
        return $pusher->trigger($request->channel, $request->event, $request->data);
    }

}