# Laravel Pusher Relay
####Tested for Laravel 5.x only

This a package to turn a simple post request into a Pusher Notification with Laravel 5. It contains a controller to register in your routes file. It uses the normal Laravel Broadcasting configuration settings for using pusher.

This package makes use of [Pusher's PHP Server](https://github.com/pusher/pusher-http=php).

# Installation
Require this package with composer:
```composer require elegasoft/pusher-relay
```
 
 Add your Pusher credentials to your `.env` file:
```PUSHER_APP_ID=PusherAppID
 PUSHER_APP_KEY=PusherAppKey
 PUSHER_APP_SECRET=PusherAppSecretKey
```
 You may need to update Laravel's default configuration:
```
 'options' => [
         'cluster' => 'eu',
         'encrypted' => true
 ]
```

# Usage 
### Register a Route

Register a route in `Routes/api.php`:
``` 
Route::post('notification', '\PusherRelay\PusherRelayController@store');
```

### Required Fields

```
$rules = [
        'channel' => 'required',
        'event' => 'required',
        'data' => 'required|array',
]
```

### Forming a post route to PusherRelay

Example using AxiosJS:
```
axios.post('http://localhost/notificatoin',[
        'channel' => 'my-channel',
        'event' => 'my-event',
        'data' => ['message'=>'Hello World']
])
```

